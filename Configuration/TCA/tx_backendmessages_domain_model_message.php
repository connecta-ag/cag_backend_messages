<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message',
        'label' => 'date',
        'label_alt' => 'title',
        'label_alt_force' => true,
        'default_sortby' => 'ORDER BY date desc',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,bodytext,date,internal_note,be_users,be_usergroups,type,comments,logs',
        'iconfile' => 'EXT:backend_messages/Resources/Public/Icons/tx_backendmessages_domain_model_message.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, bodytext, date, internal_note, be_users, be_usergroups, type, comments, logs',
    ],
    'types' => [
        '1' => ['showitem' => '
                    hidden,
                    --palette--;;type_and_date, 
                    --palette--;;title_and_expiry_date, 
                    bodytext,  internal_note, 
                    --div--;LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages.div.recipients,
                    be_users, be_usergroups,  
                    --div--;LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages.div.comments,
                    comments, 
                    --div--;LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages.div.logs,
                    logs, 
                    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, 
                    sys_language_uid, l10n_parent, l10n_diffsource
                '],
    ],
    'palettes' => [
        'type_and_date' => [
            'showitem' => 'type, date',
        ],
        'title_and_expiry_date' => [
            'showitem' => 'title, expiry_date',
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_backendmessages_domain_model_message',
                'foreign_table_where' => 'AND tx_backendmessages_domain_model_message.pid=###CURRENT_PID### AND tx_backendmessages_domain_model_message.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.hidden.activated'
                    ]
                ],
                'default' => 1
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.title',
            'config' => [
                'type' => 'input',
                'size' => 40,
                'eval' => 'trim,required'
            ],
        ],
        'date' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 12,
                'eval' => 'datetime,required',
                'default' => time()
            ],
        ],
        'expiry_date' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.expiry_date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 12,
                'eval' => 'datetime,required',
            ],
        ],
        'bodytext' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.bodytext',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'eval' => 'trim'
            ]
        ],
        'internal_note' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.internal_note',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim'
            ]
        ],
        'be_users' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.be_users',
            'config' => [
                'renderType' => 'selectMultipleSideBySide',
                'type' => 'select',
                'enableMultiSelectFilterTextfield' => true,
                'foreign_table' => 'be_users',
                'MM' => 'tx_backendmessages_message_backenduser_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
            
        ],
        'be_usergroups' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.be_usergroups',
            'config' => [
                'type' => 'select',
                'enableMultiSelectFilterTextfield' => true,
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'be_groups',
                'MM' => 'tx_backendmessages_message_backendusergroup_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
            
        ],
        'type' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => \CAG\BackendMessages\Configuration\MessageTypeConfiguration::getTcaItems()
            ],
        ],
        'comments' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.comments',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_backendmessages_domain_model_messagecomment',
                'foreign_field' => 'message',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 0,
                    'showPossibleLocalizationRecords' => 0,
                    'showAllLocalizationLink' => 0
                ],
            ],

        ],
        'logs' => [
            'exclude' => true,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_message.logs',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_backendmessages_domain_model_messagelog',
                'foreign_field' => 'message',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
    
    ],
];
