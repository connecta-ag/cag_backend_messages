<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagelog',
        'label' => 'date',
        'label_alt' => 'type',
        'label_alt_force' => true,
        'default_sortby' => 'ORDER BY date DESC',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'enablecolumns' => [
        ],
        'searchFields' => 'type,bodytext,data,date,be_user,message',
        'iconfile' => 'EXT:backend_messages/Resources/Public/Icons/tx_backendmessages_domain_model_messagelog.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, type, bodytext, data, date, be_user, message',
    ],
    'types' => [
        '1' => ['showitem' => '
                    --palette--;;date_and_type,
                    bodytext, be_user, message,
                    --div--;LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages.div.lng,
                    sys_language_uid, l10n_parent, l10n_diffsource
                '],
    ],
    'palettes' => [
        'date_and_type' => [
            'showitem' => 'date, type',
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_backendmessages_domain_model_messagelog',
                'foreign_table_where' => 'AND tx_backendmessages_domain_model_messagelog.pid=###CURRENT_PID### AND tx_backendmessages_domain_model_messagelog.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        'type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagelog.type',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'bodytext' => [
            'exclude' => true,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagelog.bodytext',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim'
            ]
        ],
        'data' => [
            'exclude' => true,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagelog.data',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'eval' => 'trim'
            ]
        ],
        'date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagelog.date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 12,
                'eval' => 'datetime',
                'default' => time()
            ],
        ],
        'be_user' => [
            'exclude' => true,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagelog.be_user',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'be_users',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],

        'message' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
