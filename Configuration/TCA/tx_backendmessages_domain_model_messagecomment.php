<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagecomment',
        'label' => 'be_user',
        'label_alt' => 'date',
        'label_alt_force' => true,
        'default_sortby' => 'ORDER BY date DESC',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
        ],
        'searchFields' => 'date,action,bodytext,data,status,be_user',
        'iconfile' => 'EXT:backend_messages/Resources/Public/Icons/tx_backendmessages_domain_model_messagecomment.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, date, action, bodytext, data, status, be_user',
    ],
    'types' => [
        '1' => ['showitem' => '
                    --palette--;;date_and_status, 
                    action, bodytext, be_user,
                    --div--;LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages.div.lng,
                    sys_language_uid, l10n_parent, l10n_diffsource
                '],
    ],
    'palettes' => [
        'date_and_status' => [
            'showitem' => 'date, status',
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_backendmessages_domain_model_messagecomment',
                'foreign_table_where' => 'AND tx_backendmessages_domain_model_messagecomment.pid=###CURRENT_PID### AND tx_backendmessages_domain_model_messagecomment.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        'date' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagecomment.date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 12,
                'eval' => 'datetime',
                'default' => time()
            ],
        ],
        'action' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagecomment.action',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'bodytext' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagecomment.bodytext',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim'
            ]
        ],
        'data' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagecomment.data',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'eval' => 'trim'
            ]
        ],
        'status' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagecomment.status',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'be_user' => [
            'exclude' => false,
            'label' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:tx_backendmessages_domain_model_messagecomment.be_user',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'be_users',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
    
        'message' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
