# Frontend plugin configuration
plugin.tx_backendmessages {
  view {
    templateRootPaths.0 = EXT:{extension.extensionKey}/Resources/Private/Templates/
    templateRootPaths.1 = {$module.tx_backendmessages.view.templateRootPath}
    partialRootPaths.0 = EXT:backend_messages/Resources/Private/Partials/
    partialRootPaths.1 = {$module.tx_backendmessages.view.partialRootPath}
    layoutRootPaths.0 = EXT:backend_messages/Resources/Private/Layouts/
    layoutRootPaths.1 = {$module.tx_backendmessages.view.layoutRootPath}
  }
  persistence {
    storagePid = {$plugin.tx_backendmessages_externalapi.persistence.storagePid}
    #recursive = 1
  }
  features {
    #skipDefaultArguments = 1
    # if set to 1, the enable fields are ignored in BE context
    ignoreAllEnableFieldsInBe = 0
    # Should be on by default, but can be disabled if all action in the plugin are uncached
    requireCHashArgumentForActionArguments = 1
  }
  mvc {
    #callDefaultActionIfActionCantBeResolved = 1
  }
}

# Module configuration
module.tx_backendmessages {
    persistence {
        storagePid = {$module.tx_backendmessages.persistence.storagePid}
    }
    view {
        templateRootPaths.0 = EXT:{extension.extensionKey}/Resources/Private/Templates/
        templateRootPaths.1 = {$module.tx_backendmessages.view.templateRootPath}
        partialRootPaths.0 = EXT:backend_messages/Resources/Private/Partials/
        partialRootPaths.1 = {$module.tx_backendmessages.view.partialRootPath}
        layoutRootPaths.0 = EXT:backend_messages/Resources/Private/Layouts/
        layoutRootPaths.1 = {$module.tx_backendmessages.view.layoutRootPath}
    }
}

# Include Pagetypes
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:backend_messages/Configuration/TypoScript/Page/" extensions="ts">
