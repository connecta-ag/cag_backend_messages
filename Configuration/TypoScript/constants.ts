
module.tx_backendmessages {
    view {
        # cat=module.tx_backendmessages/file; type=string; label=Path to template root (BE)
        templateRootPath = EXT:backend_messages/Resources/Private/Templates/
        # cat=module.tx_backendmessages/file; type=string; label=Path to template partials (BE)
        partialRootPath = EXT:backend_messages/Resources/Private/Partials/
        # cat=module.tx_backendmessages/file; type=string; label=Path to template layouts (BE)
        layoutRootPath = EXT:backend_messages/Resources/Private/Layouts/
    }
    persistence {
        # cat=module.tx_backendmessages//a; type=string; label=Default storage PID
        storagePid =
    }
}
