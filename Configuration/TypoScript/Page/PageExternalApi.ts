backendMessageExternalApi = PAGE
backendMessageExternalApi {
  typeNum = 1590502845
  10 = USER_INT
  10 {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    extensionName = BackendMessages
    pluginName = ExternalApi
    vendorName = CAG
    controller = Frontend\Message
    switchableControllerActions {
      Frontend\Message {
        1 = createBackendMessage

      }
    }
  }

  config {
    disableAllHeaderCode = 1
    additionalHeaders.10.header = Content-Type:application/json; charset=UTF-8
    xhtml_cleaning = 0
    admPanel = 0
    debug = 0
  }
}
