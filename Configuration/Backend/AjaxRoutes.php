<?php

/**
 * Definitions for routes provided by EXT:backend_messages
 */
return [
    'backendmessages_getMessages' => [
        'path' => '/backend-messages/getMessages',
        'target' => \CAG\BackendMessages\Controller\Backend\AjaxController::class . '::getMessages'
    ],
    'backendmessages_updateMessage' => [
        'path' => '/backend-messages/updateMessage',
        'target' => \CAG\BackendMessages\Controller\Backend\AjaxController::class . '::updateMessage'
    ]
];
