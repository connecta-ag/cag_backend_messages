<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'CAG.BackendMessages',
            'ExternalApi',
            [
                'Frontend\Message' => 'createBackendMessage'
            ],
            // non-cacheable actions
            [
                'Frontend\Message' => 'createBackendMessage'
            ]
        );

        $icons = [
            'tx_backendmessages_domain_model_message' => 'EXT:backend_messages/Resources/Public/Icons/tx_backendmessages_domain_model_message.svg',
            'tx_backendmessages_domain_model_messagecomment' => 'EXT:backend_messages/Resources/Public/Icons/tx_backendmessages_domain_model_messagecomment.svg',
            'tx_backendmessages_domain_model_messagelog' => 'EXT:backend_messages/Resources/Public/Icons/tx_backendmessages_domain_model_messagelog.svg',
        ];


        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            \TYPO3\CMS\Core\Imaging\IconRegistry::class
        );

        foreach ($icons as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier, // Icon-Identifier, z.B. tx-myext-action-preview
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => $path]
            );
        }

    }
);
