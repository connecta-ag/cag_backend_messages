# TYPO3 Ext:backend_messages

With the TYPO3 extension backend_messages, backend administrators can create messages for backend users.
Which user or user groups should receive the messages and how long can be defined.

## Quick installation and Use



### Installation:
- Add Repository to composer.json

````
{  
    "type": "git",
    "url": "https://bitbucket.org/connecta-ag/cag_backend_messages"
},
````
- Add ``"cag/backend-messages": "^8.7",`` to ``"require": {...}`` part of composer.json
- Or download it and install it with the classic way (Extension Manager)
- Clear caches
- Add a sysfolder for your Backend Messages and set the pid in the extension settings over Extension Manager
- Set SecretKey in Extension Settings (needed for verification of external Api-Calls)

After that you have a backend module on the left side and can immediately use the functionality

### External Api-Calls

To create Backend Messages by an external call you have following possibilities:

     * Call Uri:
     * www.your-domain-name.de/?id=1&type=1590502845&templateKey=startDeployment&accessToken=md5(date('YmdG') . $secretKey)
     *
     * Params TYPO3:
     * - id - Page Id required to avoid 404
     * - type - Page Type cofigured to execute the Api
     *
     * Params required:
     * - access token  (must be generated as follow md5(date(Y-m-d-H) . secretKey)
     * - key - Type / Template name  of predefined messages (possible values 'startDeployment','deploymentCompleted')
     *
     * Params optional:
     * - expiryDate - Expiry date of Mesage as timestamp (Date / Time)
     * - subject - Message subject
     * - text - Message text
     
For testing purposes you can set the parameter **accessToken** to value **gettokenjetzt** (&accessToken=gettokenjetzt) and you get a valid token value if you are logged in in the backend.


## Which femanager for which TYPO3 and PHP?

| backend messages | TYPO3        | PHP       | Support/Development        |
| ---------------- | ------------ | ----------|----------------------------|
| 8.x              | 8.7 - 8.7.99 | 7.1 - 7.2 |                            |
 

## Changelog
                                                                                         

## Your Contribution

## Screenshots

