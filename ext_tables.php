<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'CAG.BackendMessages',
            'ExternalApi',
            'External api'
        );


        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'CAG.BackendMessages',
                'tools', // Make module a submodule of 'tools'
                'messages', // Submodule key
                '', // Position
                [
                    'Backend\Message' => 'list, show, delete',
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:backend_messages/Resources/Public/Icons/user_mod_messages.svg',
                    'labels' => 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_messages.xlf',
                ]
            );


            /** Hook to load extension JavaScript modules */
            $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/backend.php']['constructPostProcess'][] = \CAG\BackendMessages\Hooks\LoadBackendJsModuleHook::class . '->loadRequireJsModule';

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('backend_messages', 'Configuration/TypoScript', 'Message board for backend user');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_backendmessages_domain_model_message', 'EXT:backend_messages/Resources/Private/Language/locallang_csh_tx_backendmessages_domain_model_message.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_backendmessages_domain_model_message');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_backendmessages_domain_model_messagecomment', 'EXT:backend_messages/Resources/Private/Language/locallang_csh_tx_backendmessages_domain_model_messagecomment.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_backendmessages_domain_model_messagecomment');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_backendmessages_domain_model_messagelog', 'EXT:backend_messages/Resources/Private/Language/locallang_csh_tx_backendmessages_domain_model_messagelog.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_backendmessages_domain_model_messagelog');

    }
);
