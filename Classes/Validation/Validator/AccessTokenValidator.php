<?php
namespace CAG\BackendMessages\Validation\Validator;

use CAG\BackendMessages\Configuration\MessageTemplatesConfiguration;
use CAG\BackendMessages\Value\Model\Ajax\RequestData;

/***************************************************************
 *
 *  Copyright notice
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Property Validator
 */
class AccessTokenValidator extends \CAG\BackendMessages\Validation\Validator\AbstractValidator
{

    /**
     * @param $value
     */
    public function isValid($value)
    {
        $retval = true;
        $key = (string) $value;

        $extConf = $this->request->getExtConfiguration();

        $secretKey = $extConf['secretKey'] ?? null;

        $beUserUid = $GLOBALS['BE_USER']->user['uid'] ?? 0;

        if ($secretKey) {

            $hour = (int) date('G');
            $hour2 = $hour -1;

            $secretword = date('Ymd') . $hour . $secretKey;
            $secretword2 = date('Ymd') . $hour2 . $secretKey;

            $token = md5($secretword);
            $token2 = md5($secretword2);

            if ($value != $token && $value != $token2) {
                $retval = false;
                $this->request->addError('1590650650', $this->propertyName . ' is not a valid access token', 'error');
            }

            if ($value == 'gettokenjetzt' && $beUserUid) {
                echo $token;
                exit;
            }

        } else {
            $retval = false;
            $this->request->addError('1590653005', $this->propertyName . ' is not possible to test. secretKey is in extensions settings not set.', 'error');
        }



        $beUserSession = $GLOBALS['BE_USER'];

        $userGroupsUID = $beUserSession->userGroupsUID;
        $beUserUid = $beUserSession->user['uid'] ?? 0;

        return $retval;
    }
}
