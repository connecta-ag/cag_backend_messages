<?php
namespace CAG\BackendMessages\Validation;

use CAG\BackendMessages\Value\Model\Ajax\RequestData;
use CAG\BackendMessages\Validation\Validator\ValidatorInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Validator
 */
abstract class AbstractActionValidator
{

    /**
     * @param RequestData $request
     * @param array $config
     */
    public function isValid(RequestData $request, $config)
    {
        $this->validateRequiredArguments($request, $config);
        $this->validateAllowedArguments($request, $config);

        return $request->isValid();
    }


    /**
     * @param RequestData $request
     * @param array $config
     */
    protected function validateRequiredArguments($request, $config)
    {
        $conf = $config['requiredArguments'];
        $data = $request->getArguments();
        $rootline = ['arguments'];
        $this->validateRequiredArrayStructure($request, $conf, $data, $rootline);
    }


    /**
     * @param RequestData $request
     * @param array $config
     */
    protected function validateAllowedArguments($request, $config)
    {
        $conf = $config['allowedArguments'];
        $data = $request->getArguments();
        $rootline = ['arguments'];
        $this->validateAllowedArrayStructure($request, $conf, $data, $rootline);
    }

    /**
     * @param RequestData $request
     * @param array $conf
     * @param array $data
     * @param array $rootLine
     */
    protected function validateRequiredArrayStructure($request, $conf, $data, $rootline)
    {
        $rootlineStr = implode('.', $rootline);
        $rootlineStr .= $rootlineStr ? '.' : '';

        foreach ($conf as $key => $confItem) {

            if (is_array($confItem) && sizeof($confItem)) {

                if (!is_array($data[$key])) {
                    $request->addError(
                        '1586421991',
                        'Required set of properties "' . $rootlineStr . $key . '" is not set',
                        'error'
                    );
                } else {
                    // valiadate sub items
                    $rootline[] = $key;
                    $dataItem = $data[$key];
                    $this->validateRequiredArrayStructure($request, $confItem, $dataItem, $rootline);
                }

            } else {
                if (!isset($data[$confItem])) {
                    $request->addError(
                        '1586421647',
                        'Required property "' . $rootlineStr . $confItem . '" is not set',
                        'error'
                    );
                }
            }
        }
    }

    /**
     * @param RequestData $request
     * @param array $conf
     * @param array $data
     * @param array $rootLine
     */
    protected function validateAllowedArrayStructure($request, $conf, $data, $rootline)
    {
        $rootlineStr = implode('.', $rootline);
        $rootlineStr .= $rootlineStr ? '.' : '';

        if (is_array($data)) {
            foreach ($data as $key => $dataItem) {

                $confItem = $conf[$key] ?? null;
                $propertyName = $rootlineStr . $key;

                if ($confItem) {
                    if (is_array($confItem)) {

                        if (!is_array($confItem) || $this->hasPropertyOwnSubvalidators($request, $confItem, $propertyName)) {
                            $this->validateProperty($request, $confItem, $dataItem, $propertyName);
                        } else {
                            // valiadate sub items
                            $rootline[] = $key;
                            $this->validateAllowedArrayStructure($request, $confItem, $dataItem, $rootline);
                        }

                    } else {
                        // if dataItem is not an array but $confItem is an array
                        if (is_array($confItem)) {
                            $request->addError(
                                '1587976005',
                                $propertyName . ' must have subproperties. This is no a single property',
                                'error'
                            );
                        } else {
                            $this->validateProperty($request, $confItem, $dataItem, $propertyName);
                        }
                    }
                } else {
                    $request->addError(
                        '1586503885',
                        $propertyName . ' is not allowed',
                        'error'
                    );
                }
            }
        }
    }

    /**
     * @param RequestData $request
     * @param $validatorClassName
     * @param $value
     */
    protected function validateProperty($request, $validatorClassName, $value, $propertyName)
    {
        if (class_exists($validatorClassName)) {
            $fullValidatorClassName = $validatorClassName;
        } else {
            $fullValidatorClassName = 'CAG\\BackendMessages\\Validation\\Validator\\' . $validatorClassName;
        }

        try {
            /** @var ValidatorInterface $validator */
            $validator = GeneralUtility::makeInstance($fullValidatorClassName, $request, $propertyName);
            $validator->isValid($value);

        } catch (\Throwable $e) {
            $request->addError($e->getCode(), $e->getMessage(), 'error');
        }
    }

    /**
     * @param RequestData $request
     * @param $validatorClassName
     */
    protected function hasPropertyOwnSubvalidators($request, $validatorClassName, $propertyName)
    {
        $return = false;

        // only if $validatorClassName not a conf array
        if (!is_array($validatorClassName)) {
            if (class_exists($validatorClassName)) {
                $fullValidatorClassName = $validatorClassName;
            } else {
                $fullValidatorClassName = 'CAG\\BackendMessages\\Validation\\Validator\\' . $validatorClassName;
            }

            try {
                /** @var ValidatorInterface $validator */
                $validator = GeneralUtility::makeInstance($fullValidatorClassName, $request, $propertyName);
                $return = $validator->hasOwnSubValidators();

            } catch (\Throwable $e) {
                $request->addError($e->getCode(), $e->getMessage(), 'error');
            }
        }



        return $return;
    }

}
