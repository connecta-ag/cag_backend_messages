<?php
namespace CAG\BackendMessages\Value\Model;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageType
 */
class MessageType
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * severity
     *
     * @var string
     */
    protected $severity = 'info';

    /**
     * style
     *
     * @var string
     */
    protected $style = 'alert-info';

    /**
     * icon
     *
     * @var string
     */
    protected $icon = 'fa-info';

    /**
     * isBackendBlocked
     *
     * @var bool
     */
    protected $isBackendBlocked = false;

    /**
     * duration
     *
     * @var int
     */
    protected $duration = 0;

    /**
     * MessageType constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $config = \CAG\BackendMessages\Configuration\MessageTypeConfiguration::getConfig($type);

        if ($config) {
            $title = \CAG\BackendMessages\Configuration\MessageTypeConfiguration::getLocalizedTitle($config['title_local_lang_key']);
            $this->setType($type);
            $this->setTitle($title);
            $this->setIsBackendBlocked($config['is_backend_blocked']);
            $this->setDuration($config['duration']);
            $this->setSeverity($config['severity']);
            $this->setStyle($config['style']);
            $this->setIcon($config['icon']);
        } else {
            $this->setType('undefined');
            $this->setTitle('undefined');
            $this->setIsBackendBlocked(false);
            $this->setDuration(0);
        }
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the isBackendBlocked
     *
     * @return bool $isBackendBlocked
     */
    public function getIsBackendBlocked()
    {
        return $this->isBackendBlocked;
    }

    /**
     * Sets the isBackendBlocked
     *
     * @param bool $isBackendBlocked
     * @return void
     */
    public function setIsBackendBlocked($isBackendBlocked)
    {
        $this->isBackendBlocked = $isBackendBlocked;
    }

    /**
     * Returns the boolean state of isBackendBlocked
     *
     * @return bool
     */
    public function isIsBackendBlocked()
    {
        return $this->isBackendBlocked;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getSeverity(): string
    {
        return $this->severity;
    }

    /**
     * @param string $severity
     */
    public function setSeverity(string $severity): void
    {
        $this->severity = $severity;
    }

    /**
     * @return string
     */
    public function getStyle(): string
    {
        return $this->style;
    }

    /**
     * @param string $style
     */
    public function setStyle(string $style): void
    {
        $this->style = $style;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

}
