<?php
namespace CAG\BackendMessages\Value\Model\Ajax;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Message of REST-Service
 */
class UserMessage
{

    /**
     * @var int
     */
    public $uid = 0;

    /**
     * @var string
     */
    public $type = '';

    /**
     * @var string
     */
    public $severity = '';

    /**
     * @var boolean
     */
    public $backendBlocked = false;

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $text = '';

    /**
     * @var int
     */
    public $time = 0;

    /**
     * @var int
     */
    public $expiryDate = 0;

    /**
     * @var int
     */
    public $duration = 0;

    /**
     * Message constructor.
     * @param \CAG\BackendMessages\Domain\Model\Message $message
     */
    public function __construct($message) {


        $messageType = $message->getType();
        if ($messageType) {
            $this->setType($messageType->getType());
        }

        $this->setBackendBlocked($message->getType()->isIsBackendBlocked());
        $this->setDuration($message->getType()->getDuration());
        $this->setSeverity($message->getType()->getSeverity());
        $this->setUid($message->getUid());
        $this->setTitle($message->getTitle());
        $this->setText($message->getBodytext());

        if ($message->getDate() instanceOf \DateTime) {
            $this->setTime($message->getDate()->getTimestamp());
        }

        if ($message->getExpiryDate() instanceOf \DateTime) {
            $this->setExpiryDate($message->getExpiryDate()->getTimestamp());
        }
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isBackendBlocked(): bool
    {
        return $this->backendBlocked;
    }

    /**
     * @param bool $backendBlocked
     */
    public function setBackendBlocked(bool $backendBlocked): void
    {
        $this->backendBlocked = $backendBlocked;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime(int $time): void
    {
        $this->time = $time;
    }

    /**
     * @return int
     */
    public function getExpiryDate(): int
    {
        return $this->expiryDate;
    }

    /**
     * @param int $expiryDate
     */
    public function setExpiryDate(int $expiryDate): void
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getSeverity(): string
    {
        return $this->severity;
    }

    /**
     * @param string $severity
     */
    public function setSeverity(string $severity): void
    {
        $this->severity = $severity;
    }

}
