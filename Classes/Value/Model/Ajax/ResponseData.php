<?php
namespace CAG\BackendMessages\Value\Model\Ajax;

/***************************************************************
 *
 *  Copyright notice
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Response of REST-Service
 */
class ResponseData
{

    const PROCESSING_FAILED = 'processing_failed';
    const PROCESSING_OK = 'processing_ok';


    /**
     * @var bool
     */
    public $success = false;

    /**
     * @var string
     */
    public $status = '';

    /**
     * @var array<\CAG\BackendMessages\Value\Model\Ajax\Message>
     */
    public $errors = [];

    /**
     * @var array<\CAG\BackendMessages\Value\Model\Ajax\Message>
     */
    public $messages = [];

    /**
     * @var string
     */
    public $action = '';

    /**
     * @var array
     */
    public $result = [];

    /**
     * @var array
     */
    public $args = [];

    public function __construct($action)
    {
        $this->setAction($action);
    }


    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    protected function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @return array<Error>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array<Error> $error
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @param array $args
     */
    public function setArgs(array $args): void
    {
        $this->args = $args;
    }


    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }

    /**
     * @param array $result
     */
    public function setResult(array $result): void
    {
        $this->result = $result;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @param array $messages
     */
    public function setMessages(array $messages): void
    {
        $this->messages = $messages;
    }

    /**
     * @param string $code
     * @param string $message
     * @param string $code
     */
    public function addMessage($code, $message, $type): void
    {
        $this->messages[] = new Message($code, $message, $type);
    }

    /**
     * @param \CAG\BackendMessages\Value\Model\Ajax\Message $error
     * @param string $code
     */
    public function addError($code, $message, $type): void
    {
        $this->errors[] = new Message($code, $message, $type);
    }

}
