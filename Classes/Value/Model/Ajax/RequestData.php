<?php
namespace CAG\BackendMessages\Value\Model\Ajax;

use CAG\LtoRest\Utility\OptionsUtilityInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * RequestData
 */
class RequestData
{

    /**
     * @var string
     */
    protected $action = '';

    /**
     * @var array<Message>
     */
    protected $errors = [];

    /**
     * @var array
     */
    protected $arguments = [];

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * @var array
     */
    protected $extConfiguration = [];


    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return sizeof($this->errors) ? false : true;
    }


    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array<Message> $error
     */
    public function setErrors(array $error): void
    {
        $this->errors = $error;
    }

    /**
     * @param Message $error
     */
    /**
     * @param string $message
     * @param string $code
     */
    public function addError($code, $message, $type): void
    {
        $this->errors[] = new Message($code, $message, $type);
    }


    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     */
    public function setArguments(array $arguments): void
    {
        $this->arguments = $arguments;
    }

    /**
     * @return array
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings(array $settings): void
    {
        $this->settings = $settings;
    }

    /**
     * @return array
     */
    public function getExtConfiguration(): array
    {
        return $this->extConfiguration;
    }

    /**
     * @param array $extConfiguration
     */
    public function setExtConfiguration(array $extConfiguration): void
    {
        $this->extConfiguration = $extConfiguration;
    }

}
