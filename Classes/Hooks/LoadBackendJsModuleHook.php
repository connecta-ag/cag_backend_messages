<?php
namespace CAG\BackendMessages\Hooks;

use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageController
 */
class LoadBackendJsModuleHook
{
    /**
     * @param $hookConfiguration
     * @param \TYPO3\CMS\Backend\Controller $backendController
     */
    public function loadRequireJsModule($hookConfiguration, $backendController)
    {
        /** @var  $renderer \TYPO3\CMS\Core\Page\PageRenderer */
        $renderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class);

        // Load our message modul
        $renderer->loadRequireJsModule(
            'TYPO3/CMS/BackendMessages/UserMessagesModule'
        );

        // Load our message modul
        $renderer->loadRequireJsModule(
            'TYPO3/CMS/BackendMessages/UserMessages'
        );
    }
}
