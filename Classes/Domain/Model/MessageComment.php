<?php
namespace CAG\BackendMessages\Domain\Model;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageComment
 */
class MessageComment extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * date
     *
     * @var \DateTime
     */
    protected $date = null;

    /**
     * action
     *
     * @var string
     */
    protected $action = '';

    /**
     * bodytext
     *
     * @var string
     */
    protected $bodytext = '';

    /**
     * data
     *
     * @var string
     */
    protected $data = '';

    /**
     * status
     *
     * @var string
     */
    protected $status = '';

    /**
     * beUser
     *
     * @var \CAG\BackendMessages\Domain\Model\BackendUser
     * @lazy
     */
    protected $beUser = null;

    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Returns the action
     *
     * @return string $action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Sets the action
     *
     * @param string $action
     * @return void
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * Returns the bodytext
     *
     * @return string $bodytext
     */
    public function getBodytext()
    {
        return $this->bodytext;
    }

    /**
     * Sets the bodytext
     *
     * @param string $bodytext
     * @return void
     */
    public function setBodytext($bodytext)
    {
        $this->bodytext = $bodytext;
    }

    /**
     * Returns the data
     *
     * @return string $data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the data
     *
     * @param string $data
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Returns the status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     *
     * @param string $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Returns the beUser
     *
     * @return \CAG\BackendMessages\Domain\Model\BackendUser $beUser
     */
    public function getBackendUser()
    {
        return $this->beUser;
    }

    /**
     * Sets the beUser
     *
     * @param \CAG\BackendMessages\Domain\Model\BackendUser $beUser
     * @return void
     */
    public function setBackendUser(\CAG\BackendMessages\Domain\Model\BackendUser $beUser)
    {
        $this->beUser = $beUser;
    }
}
