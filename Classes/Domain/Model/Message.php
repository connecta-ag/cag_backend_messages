<?php
namespace CAG\BackendMessages\Domain\Model;

use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * Message
 */
class Message extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * bodytext
     *
     * @var string
     */
    protected $bodytext = '';

    /**
     * date
     *
     * @var \DateTime
     * @validate NotEmpty
     */
    protected $date = null;

    /**
     * expiry_date
     *
     * @var \DateTime
     */
    protected $expiryDate = null;

    /**
     * internalNote
     *
     * @var string
     */
    protected $internalNote = '';

    /**
     * beUsers
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\BackendUser>
     */
    protected $beUsers = null;

    /**
     * backendUserGroups
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\BackendUsergroup>
     */
    protected $backendUserGroups = null;

    /**
     * type
     *
     * @var string
     */
    protected $type = null;

    /**
     * comments
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\MessageComment>
     * @cascade remove
     */
    protected $comments = null;

    /**
     * logs
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\MessageLog>
     * @cascade remove
     */
    protected $logs = null;

    /**
     * @var bool
     */
    protected $hidden = true;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->beUsers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->backendUserGroups = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->comments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->logs = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the bodytext
     *
     * @return string $bodytext
     */
    public function getBodytext()
    {
        return $this->bodytext;
    }

    /**
     * Sets the bodytext
     *
     * @param string $bodytext
     * @return void
     */
    public function setBodytext($bodytext)
    {
        $this->bodytext = $bodytext;
    }

    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return \DateTime
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param \DateTime $expiryDate
     */
    public function setExpiryDate(\DateTime $expiryDate): void
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * Returns the internalNote
     *
     * @return string $internalNote
     */
    public function getInternalNote()
    {
        return $this->internalNote;
    }

    /**
     * Sets the internalNote
     *
     * @param string $internalNote
     * @return void
     */
    public function setInternalNote($internalNote)
    {
        $this->internalNote = $internalNote;
    }

    /**
     * Adds a BackendUser
     *
     * @param \CAG\BackendMessages\Domain\Model\BackendUser $beUser
     * @return void
     */
    public function addBackendUser(\CAG\BackendMessages\Domain\Model\BackendUser $beUser)
    {
        $this->beUsers->attach($beUser);
    }

    /**
     * Removes a BackendUser
     *
     * @param \CAG\BackendMessages\Domain\Model\BackendUser $beUserToRemove The BackendUser to be removed
     * @return void
     */
    public function removeBackendUser(\CAG\BackendMessages\Domain\Model\BackendUser $beUserToRemove)
    {
        $this->beUsers->detach($beUserToRemove);
    }

    /**
     * Returns the beUsers
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\BackendUser> $beUsers
     */
    public function getBackendUsers()
    {
        return $this->beUsers;
    }

    /**
     * Sets the beUsers
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\BackendUser> $beUsers
     * @return void
     */
    public function setBackendUsers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $beUsers)
    {
        $this->beUsers = $beUsers;
    }

    /**
     * Adds a BackendUsergroup
     *
     * @param \CAG\BackendMessages\Domain\Model\BackendUsergroup $backendUserGroup
     * @return void
     */
    public function addBackendUsergroup(\CAG\BackendMessages\Domain\Model\BackendUsergroup $backendUserGroup)
    {
        $this->backendUserGroups->attach($backendUserGroup);
    }

    /**
     * Removes a BackendUsergroup
     *
     * @param \CAG\BackendMessages\Domain\Model\BackendUsergroup $backendUserGroupToRemove The BackendUsergroup to be removed
     * @return void
     */
    public function removeBackendUsergroup(\CAG\BackendMessages\Domain\Model\BackendUsergroup $backendUserGroupToRemove)
    {
        $this->backendUserGroups->detach($backendUserGroupToRemove);
    }

    /**
     * Returns the backendUserGroups
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\BackendUsergroup> $backendUserGroups
     */
    public function getBackendUsergroups()
    {
        return $this->backendUserGroups;
    }

    /**
     * Sets the backendUserGroups
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\BackendUsergroup> $backendUserGroups
     * @return void
     */
    public function setBackendUsergroups(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $backendUserGroups)
    {
        $this->backendUserGroups = $backendUserGroups;
    }

    /**
     * Returns the type
     *
     * @return \CAG\BackendMessages\Value\Model\MessageType $type
     */
    public function getType()
    {
        $messageType = GeneralUtility::makeInstance(\CAG\BackendMessages\Value\Model\MessageType::class, $this->type);
        return $messageType;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * Adds a MessageComment
     *
     * @param \CAG\BackendMessages\Domain\Model\MessageComment $comment
     * @return void
     */
    public function addComment(\CAG\BackendMessages\Domain\Model\MessageComment $comment)
    {
        $this->comments->attach($comment);
    }

    /**
     * Removes a MessageComment
     *
     * @param \CAG\BackendMessages\Domain\Model\MessageComment $commentToRemove The MessageComment to be removed
     * @return void
     */
    public function removeComment(\CAG\BackendMessages\Domain\Model\MessageComment $commentToRemove)
    {
        $this->comments->detach($commentToRemove);
    }

    /**
     * Returns the comments
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\MessageComment> $comments
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Sets the comments
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\MessageComment> $comments
     * @return void
     */
    public function setComments(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $comments)
    {
        $this->comments = $comments;
    }

    /**
     * Adds a MessageLog
     *
     * @param \CAG\BackendMessages\Domain\Model\MessageLog $log
     * @return void
     */
    public function addLog(\CAG\BackendMessages\Domain\Model\MessageLog $log)
    {
        $this->logs->attach($log);
    }

    /**
     * Removes a MessageLog
     *
     * @param \CAG\BackendMessages\Domain\Model\MessageLog $logToRemove The MessageLog to be removed
     * @return void
     */
    public function removeLog(\CAG\BackendMessages\Domain\Model\MessageLog $logToRemove)
    {
        $this->logs->detach($logToRemove);
    }

    /**
     * Returns the logs
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\MessageLog> $logs
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Sets the logs
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CAG\BackendMessages\Domain\Model\MessageLog> $logs
     * @return void
     */
    public function setLogs(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $logs)
    {
        $this->logs = $logs;
    }

    public function isExpired()
    {
        $retval = false;
        $expiryDate = $this->getExpiryDate();

        if ($expiryDate instanceof \DateTime) {
            if (time() > $expiryDate->getTimestamp()) {
                $retval = true;
            }
        }

        return $retval;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }

}
