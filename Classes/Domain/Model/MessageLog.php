<?php
namespace CAG\BackendMessages\Domain\Model;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageLog
 */
class MessageLog extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * bodytext
     *
     * @var string
     */
    protected $bodytext = '';

    /**
     * data
     *
     * @var string
     */
    protected $data = '';

    /**
     * date
     *
     * @var \DateTime
     */
    protected $date = null;

    /**
     * beUser
     *
     * @var \CAG\BackendMessages\Domain\Model\BackendUser
     * @lazy
     */
    protected $beUser = null;

    /**
     * message
     *
     * @var \CAG\BackendMessages\Domain\Model\Message
     * @lazy
     */
    protected $message = null;

    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the bodytext
     *
     * @return string $bodytext
     */
    public function getBodytext()
    {
        return $this->bodytext;
    }

    /**
     * Sets the bodytext
     *
     * @param string $bodytext
     * @return void
     */
    public function setBodytext($bodytext)
    {
        $this->bodytext = $bodytext;
    }

    /**
     * Returns the data
     *
     * @return string $data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the data
     *
     * @param string $data
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Returns the beUser
     *
     * @return \CAG\BackendMessages\Domain\Model\BackendUser $beUser
     */
    public function getBackendUser()
    {
        return $this->beUser;
    }

    /**
     * Sets the beUser
     *
     * @param \CAG\BackendMessages\Domain\Model\BackendUser $beUser
     * @return void
     */
    public function setBackendUser(\CAG\BackendMessages\Domain\Model\BackendUser $beUser)
    {
        $this->beUser = $beUser;
    }

    /**
     * Returns the message
     *
     * @return \CAG\BackendMessages\Domain\Model\Message $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Sets the message
     *
     * @param \CAG\BackendMessages\Domain\Model\Message $message
     * @return void
     */
    public function setMessage(\CAG\BackendMessages\Domain\Model\Message $message)
    {
        $this->message = $message;
    }
}
