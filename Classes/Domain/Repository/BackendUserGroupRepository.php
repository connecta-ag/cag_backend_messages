<?php
namespace CAG\BackendMessages\Domain\Repository;



/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * The repository for BackendUsergroups
 */
class BackendUserGroupRepository extends \TYPO3\CMS\Beuser\Domain\Repository\BackendUserGroupRepository
{
    }
