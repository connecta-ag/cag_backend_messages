<?php
namespace CAG\BackendMessages\Domain\Repository;

use CAG\BackendMessages\Domain\Model\Message;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * The repository for Messages
 */
class MessageRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    protected $defaultOrderings = array(

        'expiryDate'=>\TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
        'date'=>\TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
    );

    /**
     * Initializes the repository.
     */
    public function initializeObject()
    {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);

    }


    public function findAllIgnoreEnableFields()
    {
        $query = $this->createQuery();

        $query->getQuerySettings()->setIgnoreEnableFields(true);

        return $query->execute();
    }

    /**
     * @param int $beUserUid
     * @param array<integer> $userGroupsUID
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllNotShownByBackendUser($beUserUid, $userGroupsUID)
    {
        $query = $this->createQuery();

        $constrains = [];

        $constrains[] = $this->buildConstraintRecipients($query, $beUserUid, $userGroupsUID);
        $constrains[] = $this->buildConstraintExpiryDate($query);

        $messageUidsAlreadyShown = $this->getMessageUidsAlreadyShown($beUserUid, $userGroupsUID);



        if (sizeof($messageUidsAlreadyShown)) {
            $constrains[] =  $query->logicalNot($query->in('uid', $messageUidsAlreadyShown));
        }

        $query->matching($query->logicalAnd($constrains));
        $result = $query->execute();


        //        // debbug part
        //        DebuggerUtility::var_dump($messageUidsAlreadyShown, '$messageUidsAlreadyShown');
        //        DebuggerUtility::var_dump($beUserUid, '$beUserUid');
        //
        //        /** @var Message $item */
        //        foreach ($result as $item) {
        //            DebuggerUtility::var_dump($item->getTitle());
        //        }
        //        DebuggerUtility::var_dump($result->count());
        //        exit;

        return $result;
    }

    /**
     * @param int $beUserUid
     * @param array<integer> $userGroupsUID
     * @return array
     */
    public function getMessageUidsAlreadyShown($beUserUid, $userGroupsUID)
    {

        $return = [];
        $query = $this->createQuery();

        $constrains = [];
        $constrains[] = $this->buildConstraintRecipients($query, $beUserUid, $userGroupsUID);
        $constrains[] = $this->buildConstraintExpiryDate($query);
        $constrains[] = $query->equals('comments.beUser', $beUserUid);

        $query->matching($query->logicalAnd($constrains));
        $result = $query->execute(true);


        foreach ($result as $row) {
            $return[] = $row['uid'];
        }

        return $return;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\Query $query
     * @return mixed
     */
    protected function buildConstraintExpiryDate($query)
    {
        $time = time();
        $constrains = [];
        $constrains[] = $query->greaterThanOrEqual('expiryDate', $time);
        $constrains[] = $query->equals('expiryDate', 0);
        return $query->logicalOr($constrains);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\Query $query
     * @param uid $beUserUid
     * @param array $userGroupsUID
     * @return mixed
     */
    protected function buildConstraintRecipients($query, $beUserUid, $userGroupsUID)
    {
        $constrainsRecipients = [];
        $constrainsRecipients[] = $this->buildConstraintsBeUsers($query, $beUserUid);
        $constrainsRecipients[] = $this->buildConstraintsBeUsergroups($query, $userGroupsUID);

        return $query->logicalOr($constrainsRecipients);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\Query $query
     * @param int $beUserUid
     */
    protected function buildConstraintsBeUsers($query, $beUserUid)
    {
        $constrainsBeUsers = [];

        $constrainsBeUsers[] = $query->equals('beUsers', '');
        $constrainsBeUsers[] = $query->contains('beUsers', $beUserUid);


        return $query->logicalOr($constrainsBeUsers);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\Query $query
     * @param array $userGroupsUID
     */
    protected function buildConstraintsBeUsergroups($query, $userGroupsUID)
    {
        $constrainsBeUsergroups = [];

        if (sizeof($userGroupsUID)) {
            // Message has no usergroups assigned all user are allowed
            $constrainsBeUsergroups[] = $query->equals('beUsergroups', '');

            foreach ($userGroupsUID as $groupUid) {
                $constrainsBeUsergroups[] = $query->contains('beUsergroups', $groupUid);
            }

        } else {
            # if current user has no usergroups but message has some usergroups assigned
            $constrainsBeUsergroups[] = $query->logicalNot($query->equals('beUsergroups', ''));
        }


        return $query->logicalOr($constrainsBeUsergroups);
    }

}
