<?php
namespace CAG\BackendMessages\Utility;

use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageController
 */
class ExtensionConfigurationUtility
{

    private static $extName = 'backend_messages';

    /**
     * @param string $confKey
     */
    public static function getConfigurationProperty($confKey)
    {
        $extensionConfiguration = self::getConfiguration();

        return $extensionConfiguration[$confKey] ?? null;

    }

    /**
     * @return array
     */
    public static function getConfiguration()
    {
        /** @var ObjectManager $objManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var \TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility $configurationUtility */
        $configurationUtility = $objectManager->get(\TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility::class);

        $extensionConfiguration = $configurationUtility->getCurrentConfiguration(self::$extName);

        $configuration = [];

        foreach ($extensionConfiguration as $item) {

            $propertyName = $item['name'];
            $configuration[$propertyName] = $item['value'];
        }

        return $configuration;
    }
}
