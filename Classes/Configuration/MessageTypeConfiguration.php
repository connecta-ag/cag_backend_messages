<?php
namespace CAG\BackendMessages\Configuration;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageType
 */
class MessageTypeConfiguration
{
    /**
     * options:
     *
     * string title_local_lang_key - local lang key from locallang_db.xlf
     * boolean is_backend_blocked - shold backend be blocked (modal message)
     * integer duration - how long in sec schould be shown the message (info message)
     *
     * @var array
     */
    static private $config = [
        'modal_info' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.modal_info',
            'is_backend_blocked' => true,
            'severity' => 'info',
            'style' => 'alert-info',
            'icon' => 'fa-info',
            'duration' => 0,
        ],
        'modal_ok' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.modal_ok',
            'is_backend_blocked' => true,
            'severity' => 'ok',
            'style' => 'alert-success',
            'icon' => 'fa-check',
            'duration' => 0,
        ],
        'modal_warning' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.modal_warning',
            'is_backend_blocked' => true,
            'severity' => 'warning',
            'style' => 'alert-warning',
            'icon' => 'fa-exclamation',
            'duration' => 0,
        ],
        'modal_notice' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.modal_notice',
            'is_backend_blocked' => true,
            'severity' => 'notice',
            'style' => 'alert-notice',
            'icon' => 'fa-lightbulb-o',
            'duration' => 0,
        ],
        'modal_error' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.modal_error',
            'is_backend_blocked' => true,
            'severity' => 'error',
            'style' => 'alert-danger',
            'icon' => 'fa-times',
            'duration' => 0,
        ],
        'notification_info' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.notification_info',
            'is_backend_blocked' => false,
            'severity' => 'info',
            'style' => 'alert-info',
            'icon' => 'fa-info',
            'duration' => 0,
        ],
        'notification_ok' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.notification_ok',
            'is_backend_blocked' => false,
            'severity' => 'ok',
            'style' => 'alert-success',
            'icon' => 'fa-check',
            'duration' => 0,
        ],
        'notification_notice' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.notification_notice',
            'is_backend_blocked' => false,
            'severity' => 'notice',
            'style' => 'alert-notice',
            'icon' => 'fa-lightbulb-o',
            'duration' => 0,
        ],
        'notification_warning' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.notification_warning',
            'is_backend_blocked' => false,
            'severity' => 'warning',
            'style' => 'alert-warning',
            'icon' => 'fa-exclamation',
            'duration' => 0,
        ],
        'notification_error' => [
            'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.notification_error',
            'is_backend_blocked' => false,
            'severity' => 'error',
            'style' => 'alert-danger',
            'icon' => 'fa-times',
            'duration' => 0,
        ],
        // 'confirmation' => [
        //     'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.confirmation',
        //     'is_backend_blocked' => true,
        //     'duration' => 0,
        // ],
        // 'question' => [
        //     'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.question',
        //     'is_backend_blocked' => true,
        //     'duration' => 0,
        // ],
        // 'feedback' => [
        //       'title_local_lang_key' => 'tx_backendmessages_value_model_messagetype.feedback',
        //       'is_backend_blocked' => true,
        //       'duration' => 0,
        // ],
    ];

    /**
     * @param string $type
     * @return mixed|null
     */
    static public function getConfig(string $type)
    {
        return self::$config[$type] ?? null;
    }

    static public function getLocalizedTitle($llKey)
    {
        $llpath = 'LLL:EXT:backend_messages/Resources/Private/Language/locallang_db.xlf:';
        return LocalizationUtility::translate($llpath . $llKey) ?? $llKey;
    }


    /**
     * used in EXT:backend_messages/Configuration/TCA/tx_backendmessages_domain_model_message.php
     *
     * @return array
     */
    static public function getTcaItems()
    {
        $items = [];

        foreach (self::$config as $key => $confItem) {

            $title = self::getLocalizedTitle($confItem['title_local_lang_key']);

            $items[] = [
                $title,
                $key
            ];
        }

        return $items;
    }
}
