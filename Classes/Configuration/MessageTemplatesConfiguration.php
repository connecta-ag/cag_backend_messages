<?php
namespace CAG\BackendMessages\Configuration;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageTemplatesConfiguration
 */
class MessageTemplatesConfiguration
{

    private static $templates = [

        'startDeployment' => [
            'type' => 'modal_warning',
            'lifetime' => 3600, // default value if not transmitted
            'subject' => 'Deployment will be done shortly',
            'message' => 'In the next few minutes, deployment will be carried out, server will be restarted. Save your data promptly so that it is not lost.'
        ],

        'deploymentCompleted' => [
            'type' => 'modal_ok',
            'lifetime' => 3600, // default value if not transmitted
            'subject' => 'Deployment done',
            'message' => 'Deployment is done. You can continue to edit contents normally again'
        ],


    ];

    static public function getMessageTemplate($key)
    {
        return self::$templates[$key] ?? null;
    }

    static public function isMessageTemplateSet($key)
    {
        return isset(self::$templates[$key]) ? true : false;
    }

}
