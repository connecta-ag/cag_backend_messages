<?php
namespace CAG\BackendMessages\Controller\Backend;

use CAG\BackendMessages\Domain\Model\Message;
use CAG\BackendMessages\Domain\Model\MessageComment;
use CAG\BackendMessages\Domain\Repository\BackendUserRepository;
use CAG\BackendMessages\Domain\Repository\MessageRepository;
use CAG\BackendMessages\Value\Model\Ajax\ResponseData;
use CAG\BackendMessages\Value\Model\Ajax\UserMessage;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageController
 */
class AjaxController
{

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * messageRepository
     *
     * @var \CAG\BackendMessages\Domain\Repository\MessageRepository
     */
    protected $messageRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     */
    protected $persistenceManager = null;

    /**
     * backendUserRepository
     *
     * @var \CAG\BackendMessages\Domain\Repository\BackendUserRepository
     */
    protected $backendUserRepository = null;


    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->messageRepository = $this->objectManager->get(MessageRepository::class);
        $this->backendUserRepository = $this->objectManager->get(BackendUserRepository::class);
        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);
    }


    /**
     * Update a single solr connection
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function getMessages(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $result = [
            'count' => 0,
            'items' => [],
        ];


//        /** @var \TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility $configurationUtility */
//        $configurationUtility = $this->objectManager->get(\TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility::class);
//        $extensionConfiguration = $configurationUtility->getCurrentConfiguration('backend_messages');
//        $storagePid = (integer) $extensionConfiguration['storagePid']['value'] ?? 0;


        $resposnseData = GeneralUtility::makeInstance(ResponseData::class, 'getMessages');

        // $queryParams = $request->getQueryParams();
        // $pageId = 0;

        /** @var \TYPO3\CMS\Core\Authentication\BackendUserAuthentication $beUserSession */
        $beUserSession = $GLOBALS['BE_USER'];

        $userGroupsUID = $beUserSession->userGroupsUID;
        $beUserUid = $beUserSession->user['uid'] ?? 0;

        $messages = $this->messageRepository->findAllNotShownByBackendUser($beUserUid, $userGroupsUID);

        if ($messages) {

            $result['count'] = $messages->count();

            foreach ($messages as $message) {
                $result['items'][] = GeneralUtility::makeInstance(UserMessage::class, $message);
            }
        }

        $resposnseData->setSuccess(true);
        $resposnseData->setStatus(Responsedata::PROCESSING_OK);
        $resposnseData->setResult($result);

        $response->getBody()->write(json_encode($resposnseData));
        return $response;
    }

    /**
     * Update all connections
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function updateMessage(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        /** @var ResponseData  $resposnseData */
        $resposnseData = GeneralUtility::makeInstance(ResponseData::class, 'updateMessage');

        //$requestParams = $request->getServerParams();

        $body = $request->getBody();
        if ($body){
            $requestData = json_decode($body, true);

            if (isset($requestData['uid']) && is_integer($requestData['uid'])) {

                $uidMessage = $requestData['uid'] ?? 0;

                /** @var Message $message */
                $message = $this->messageRepository->findByUid($uidMessage);

                if ($message) {
                    $messageAction = $requestData['action'];
                    $messageStatus = $requestData['status'];

                    $this->addCommentToMessage($message, $messageAction, $messageStatus);
                }

            }

        }


        $resposnseData->setSuccess(true);
        $resposnseData->setStatus(Responsedata::PROCESSING_OK);

        $response->getBody()->write(json_encode($resposnseData));
        return $response;
    }

    /**
     * @param Message $message
     * @param string $action
     * @param string $status
     * @param string $message
     * @throws \Exception
     */
    protected function addCommentToMessage($message, $action, $status, $commentBodyText='') {

        /** @var \TYPO3\CMS\Core\Authentication\BackendUserAuthentication $beUser */
        $beUserSession = $GLOBALS['BE_USER'];

        $beUserUid = $beUserSession->user['uid'] ?? 0;

        if ($beUserUid) {

            $beUser = $this->backendUserRepository->findByUid($beUserUid);

            /** @var MessageComment $comment */
            $comment = GeneralUtility::makeInstance(MessageComment::class);

            $comment->setBackendUser($beUser);

            $curentDate = new \DateTime();
            $comment->setDate($curentDate);
            $comment->setAction($action);
            $comment->setStatus($status);

            if ($commentBodyText) {
                $comment->setBodytext($status);
            }

            $message->addComment($comment);

            $this->persistenceManager->update($message);
            $this->persistenceManager->persistAll();

        }

    }

}
