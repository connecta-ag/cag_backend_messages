<?php
namespace CAG\BackendMessages\Controller\Backend;

use CAG\BackendMessages\Utility\ExtensionConfigurationUtility;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageController
 */
class MessageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * messageRepository
     *
     * @var \CAG\BackendMessages\Domain\Repository\MessageRepository
     * @inject
     */
    protected $messageRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $storagePid = ExtensionConfigurationUtility::getConfigurationProperty('storagePid') ?? 0;

        if (!$storagePid) {
            $this->addFlashMessage('No storage pid is in extconf defined', 'Wrong Extconf', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        }

        $messages = $this->messageRepository->findAllIgnoreEnableFields();

        $this->view->assign('messages', $messages);
        $this->view->assign('storagePid', $storagePid);
    }

    /**
     * action show
     *
     * @param \CAG\BackendMessages\Domain\Model\Message $message
     * @return void
     */
    public function showAction(\CAG\BackendMessages\Domain\Model\Message $message)
    {
        $this->view->assign('message', $message);
    }

    /**
     * action delete
     *
     * @param \CAG\BackendMessages\Domain\Model\Message $message
     * @return void
     */
    public function deleteAction(\CAG\BackendMessages\Domain\Model\Message $message)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->messageRepository->remove($message);
        $this->redirect('list');
    }
}
