<?php
namespace CAG\BackendMessages\Controller\Frontend;

use CAG\BackendMessages\Configuration\MessageParamsConfiguration;
use CAG\BackendMessages\Configuration\MessageTemplatesConfiguration;
use CAG\BackendMessages\Domain\Model\Message;
use CAG\BackendMessages\Utility\ExtensionConfigurationUtility;
use CAG\BackendMessages\Value\Model\Ajax\RequestData;
use CAG\BackendMessages\Value\Model\Ajax\ResponseData;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility;

/***
 *
 * This file is part of the "Message board for backend user" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Connecta AG <info@connecta.ag>, Connecta AG
 *
 ***/

/**
 * MessageController
 */
class MessageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager = null;

    /**
     * messageRepository
     *
     * @var \CAG\BackendMessages\Domain\Repository\MessageRepository
     * @inject
     */
    protected $messageRepository = null;

    /**
     * @var \CAG\BackendMessages\Validation\CreateBackendMessageActionValidator
     * @inject
     */
    protected $createBackendMessageActionValidator = null;


    /**
     * action create backend message
     *
     * call .../?id=1&type=1590502845&templateKey=startDeployment&accessToken=md5(date('YmdG') . $secretKey)
     *
     * Params required:
     * - access token  (md5(date(Y-m-d-H) . secretKey)
     * - key - Type / Template Name der vordefinierten Nachricht
     *
     * Params optional:
     * - expiryDate - Verfallsdatum der Nachricht (Datum / Uhzeit Timestamp)
     * - subject - Message subject
     * - text - Message text
     *
     * @return void
     */
    public function createBackendMessageAction()
    {
        /** @var \TYPO3\CMS\Extbase\Mvc\Web\Response $resposeObj */
        $resposeObj = $this->response;

        /** @var  ResponseData $resposnseData */
        $resposnseData = GeneralUtility::makeInstance(ResponseData::class, 'createBackendMessage');

        /** @var RequestData $request */
        $request = GeneralUtility::makeInstance(RequestData::class);
        $request->setAction('createBackendMessage');
        $extConfig = $this->getExtensionConfiguration();
        $request->setExtConfiguration($extConfig);


        // check extension configuration for api is set
        // get params
        // verificate request params
        $this->retrieveCreateBackendMessageParams($request);


        if ($request->isValid()) {

            $result = [];

            // create message
            $message = $this->createBackendMessage($request);

            $result = [
                'type' => $message->getType(),
                'title' => $message->getTitle(),
                'bodytext' => $message->getBodytext(),
            ];

            if ($message->getExpiryDate()) {
                $result['expiryDate'] = $message->getExpiryDate()->getTimestamp();
                $result['expiryDateFormatted'] = $message->getExpiryDate()->format('Y-m-d H:i');
            }


            $resposnseData->setSuccess(true);
            $resposnseData->setStatus(Responsedata::PROCESSING_OK);
            $resposnseData->setResult($result);

            $resposeObj->setStatus(200);

        } else {
            $resposnseData->setSuccess(false);
            $resposnseData->setStatus(Responsedata::PROCESSING_FAILED);
            $resposnseData->setErrors($request->getErrors());

            $resposeObj->setStatus(400);
        }

        // give feedback success / not success

        return json_encode($resposnseData);
    }

    /**
     * @param RequestData $request
     * @return Message
     * @throws \Exception
     */
    protected function createBackendMessage($request) {
        // load predefined Message template

        $args = $request->getArguments();

        $tmplKey = $args['templateKey'];

        $msgTemplateConfig = MessageTemplatesConfiguration::getMessageTemplate($tmplKey);
        /** @var Message $newMessage */
        $newMessage = GeneralUtility::makeInstance(Message::class);

        $newMessage->setDate(new \DateTime());
        $newMessage->setType($msgTemplateConfig['type']);

        if (isset($args['expiryDate'])) {
            $expiryTimestamp = (int) $args['expiryDate'];
        } else {
            $lifeTime = (int) $msgTemplateConfig['lifetime'];
            $expiryTimestamp = time() + $lifeTime;
        }

        if ($expiryTimestamp) {
            $expiryDate = new \DateTime();
            $expiryDate->setTimestamp($expiryTimestamp);
            $newMessage->setExpiryDate($expiryDate);
        }

        if (isset($args['subject'])) {
            $newMessage->setTitle($args['subject']);
        } else {
            $newMessage->setTitle($msgTemplateConfig['subject']);
        }

        if (isset($args['text'])) {
            $newMessage->setBodytext($args['text']);
        } else {
            $newMessage->setBodytext($msgTemplateConfig['message']);
        }

        $newMessage->setHidden(false);

        $this->persistenceManager->add($newMessage);
        $this->persistenceManager->persistAll();

        return $newMessage;
    }

    /**
     * @param RequestData $requestData
     */
    protected function retrieveCreateBackendMessageParams($requestData) {

        $configRequestParams = MessageParamsConfiguration::getConfig();
        $paramsRaw = $_GET;
        $requestData->setArguments($paramsRaw);

        $this->createBackendMessageActionValidator->isValid($requestData, $configRequestParams);

    }

    protected function getExtensionConfiguration()
    {
        return ExtensionConfigurationUtility::getConfiguration();
    }
}
