define([
	'jquery',
	'TYPO3/CMS/BackendMessages/UserMessagesModule'
], function($, UserMessagesModule) {

	UserMessagesModule.init();

});
