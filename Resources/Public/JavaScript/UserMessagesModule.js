define([
	'jquery',
	'nprogress',
	'TYPO3/CMS/Backend/Modal',
	'TYPO3/CMS/Backend/SplitButtons',
	'TYPO3/CMS/Backend/Tooltip',
	'TYPO3/CMS/Backend/Notification',
	'TYPO3/CMS/Backend/Severity',
	'TYPO3/CMS/Core/SecurityUtility',
	'datatables',
	'TYPO3/CMS/Backend/jquery.clearable'
], function($, NProgress, Modal, SplitButtons, Tooltip, Notification, Severity, SecurityUtility) {

	var securityUtility = new SecurityUtility();

	var UserMessagesModule = {
		interval: 10000,
		uidMessageBlockedBackend: 0,
		messages: [],


		init: function() {

			var obj = this;

			obj.getMessages();

			window.setInterval(function(){
				obj.getMessages();
			}, obj.interval);

		},

		processMessages: function(messagesRequest) {

			var obj = this;

			if (messagesRequest.count > 0) {
				obj.refreshMessagesList(messagesRequest.items);
			}

			obj.processShowMessages();

		},

		processShowMessages: function() {

			var obj = this;
			var i;
			var message;

			console.log(obj.messages);

			for (i = 0; i < obj.messages.length; ++i) {

				message = obj.messages[i];

				if (message.status == 'new' && obj.uidMessageBlockedBackend == 0) {

					obj.messages[i].status = 'processed';
					obj.showMessage(message);

					if (message.backendBlocked) {
						obj.uidMessageBlockedBackend = message.uid;
						break;
					}

				} else if (obj.uidMessageBlockedBackend == message.uid) {
					// show message until ok is clicked
					obj.showMessage(message);
				}
			}

		},

		showMessage: function(message) {

			var obj = this;

			switch(message.type) {
				case 'notification_info':
				case 'notification_notice':
				case 'notification_warning':
				case 'notification_ok':
				case 'notification_error':
					obj.showNotificationMessage(message);
					break;
				case 'modal_info':
				case 'modal_notice':
				case 'modal_warning':
				case 'modal_ok':
				case 'modal_error':
					obj.showModalMessage(message);
					break;
				default:
					obj.callbackMessageNotSupported(message);
			}

		},

		refreshMessagesList: function(messages) {
			var obj = this;
			messages.forEach(function(item, index){
				if (!obj.hasMessage(item.uid)) {
					obj.addMessage(item);
				}
			});
		},

		addMessage: function(item) {
			var obj = this;
			var index = obj.messages.length;
			item.status = 'new';
			obj.messages[index] = item;
		},

		hasMessage: function(uid) {
			var obj = this;
			var retval = false;
			var i;

			for (i = 0; i < obj.messages.length; i++){
				if (obj.messages[i].uid == uid) {
					retval = true;
				}
			};

			// if no message with given uid found
			return retval;
		},

		getMessages: function() {

			var obj = this;
			var url = TYPO3.settings.ajaxUrls['backendmessages_getMessages'];
			//url += '&id=' + uid;

			if (!obj.uidMessageBlockedBackend) {
				$.ajax(url).done(function (response) {
					if (response.success) {
						obj.processMessages(response.result);
					}
				});
			} else {
				obj.processShowMessages();
			}
		},

		callbackMessageProcessed: function(message, action) {

			var obj = this;
			var url = TYPO3.settings.ajaxUrls['backendmessages_updateMessage'];

			var messageData = {
				uid: message.uid,
				status: message.status,
				type: message.type,
				severity: message.severity,
				action: action
			};

			$.ajax({
					method: "POST",
					url: url,
					data: JSON.stringify(messageData),
					contentType: "application/json; charset=utf-8",
					dataType: "json"
			}).done(function (response) {
				if (response.success) {

					console.log(response);

				} else {
					// top.TYPO3.Notification.error(
					// 	response.message
					// );
				}

			});
		},

		callbackMessageNotSupported: function(message) {
			// still not needed at time
		},

		showNotificationMessage: function(message) {

			var obj = this;
			var severity = obj.getSeverityObj(message.severity);

			top.TYPO3.Notification.showMessage(
				message.title,
				message.text,
				severity,
				message.duration
			);

			obj.callbackMessageProcessed(message, 'showNotificationMessage');
		},

		showModalMessage: function(message) {

			var obj = this;
			var severity = obj.getSeverityObj(message.severity);

			if (Modal.currentModal) {
				// console.log('modal ist active');
				// avoid show multiple modals
			} else {

				Modal.confirm(
					message.title,
					message.text,
					severity,
					[
						{
							text: TYPO3.lang['button.ok'],
							active: true,
							btnClass: 'btn-default',
							trigger: function() {
								obj.uidMessageBlockedBackend = 0;
								Modal.dismiss();
								obj.callbackMessageProcessed(message, 'showModalMessageConfirmation');
							}
						}
					]
				);
			}
		},

		getSeverityObj: function(severityKey) {

			switch(severityKey) {

				case 'info':
					return Severity.info;
				case 'notice':
					return Severity.notice;
				case 'warning':
					return Severity.warning;
				case 'ok':
					return Severity.ok;
				case 'error':
					return Severity.error;
				default:
					return Severity.info;

			}
		},

		lastProperty: 'dontsetlastcommaafterthatproperty :-)'

	};

	// To let the module be a dependency of another module, we return our object
	return UserMessagesModule;
});
